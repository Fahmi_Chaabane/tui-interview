### Note

- This repository makes use `react-hook-form` to manipulate the dynamic forms input validation.
- This repository contains a test file `test/utils/helpers.test.ts` that test typescript code written in `utils/helpers.ts`, running `npm test` should run it.
