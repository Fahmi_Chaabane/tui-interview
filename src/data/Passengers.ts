import { Gender } from "../types/Gender";
import { Passenger } from "../types/Passenger";
import { Title } from "../types/Title";

export const passengers: Record<number, Passenger> = {
  0: {
    title: Title.MRS,
    gender: Gender.FEMALE,
    firstName: "Jane",
    lastName: "Doe",
    "date-of-birth": "2003-08-31",
  },
  1: {
    title: Title.MR,
    gender: Gender.MALE,
    firstName: "John",
    lastName: "Doe",
    "date-of-birth": "2001-04-12",
  },
};
