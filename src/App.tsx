import { useState } from "react";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Container,
  Typography,
  Button,
  Box,
  Stack,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Groups2OutlinedIcon from "@mui/icons-material/Groups2Outlined";
import { useForm } from "react-hook-form";
import { passengers } from "./data/Passengers";
import PassengerForm from "./components/PassengerForm";
import { setupAgesCategories } from "./utils/helpers";
import ConfirmationDialog from "./components/ConfirmationDialog";
import { isEqual } from "lodash";

function App() {
  const {
    register,
    control,
    formState: { errors },
    setError,
    setValue,
    getValues,
    watch,
  } = useForm({ defaultValues: passengers });
  const [open, setOpen] = useState(false);

  const isValid = Object.values(errors).every(
    (p) => p && Object.values(p).every((pr) => !pr.message)
  );
  const isNotModified = isEqual(watch(), passengers);

  const agesCategories = setupAgesCategories(passengers);

  const handleClickOpen = () => {
    setOpen(true);
  };

  return (
    <Container maxWidth="md">
      <Accordion color="black" sx={{ border: "0.3rem solid black" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Stack direction="row" alignItems="center" spacing={3}>
            <Box bgcolor="#e2f3fe" borderRadius="50%" p={1}>
              <Groups2OutlinedIcon />
            </Box>

            <Typography fontWeight={600} color="#1b115c" fontSize="1.2rem">
              PASSENGER DETAILS
            </Typography>
            <Stack
              direction="row"
              alignItems="center"
              spacing={2}
              color="#1b115c"
            >
              <Typography>{agesCategories.adult} Adult</Typography>
              <Typography>{agesCategories.child} Child</Typography>
              <Typography>{agesCategories.infant} Infant</Typography>
            </Stack>
          </Stack>
        </AccordionSummary>
        <AccordionDetails sx={{ bgcolor: "#e2f3fe" }}>
          {Object.entries(passengers).map(([key, value]) => (
            <div key={key}>
              <PassengerForm
                passenger={value}
                index={key}
                register={register}
                control={control}
                errors={errors}
                setError={setError}
                setValue={setValue}
              />
            </div>
          ))}
          <Stack direction="row" justifyContent="space-between">
            <Button
              variant="contained"
              size="large"
              disabled
              sx={{ bgcolor: "#1b115c", width: "30%", py: 1.5 }}
            >
              Back
            </Button>

            <Button
              variant="contained"
              size="large"
              onClick={handleClickOpen}
              disabled={!isValid || isNotModified}
              sx={{ width: "30%", py: 1.5 }}
            >
              Next
            </Button>
            <ConfirmationDialog
              open={open}
              setOpen={setOpen}
              originalPassengers={passengers}
              newPassagers={getValues()}
            />
          </Stack>
        </AccordionDetails>
      </Accordion>
    </Container>
  );
}

export default App;
