import { passengers } from "../../data/Passengers";
import {
  calculateAge,
  countChangesInString,
  setupAgesCategories,
} from "../../utils/helpers";

describe("Testing helpers.ts file", () => {
  it("When calculateAge Called with 2003-08-31 Should Return Correct Value 20", () => {
    const date = "2003-08-31";
    const age = calculateAge(date);
    expect(age).toBe(20);
  });

  it("When setupAgesCategories Called with passengers data Should Return Correct Object", () => {
    const agesCategories = setupAgesCategories(passengers);
    expect(agesCategories).toEqual({
      adult: 2,
      child: 0,
      infant: 0,
    });
  });

  it("When countChangesInString Called with 4 differences Should Return Correct Value 4", () => {
    const stringOne = "Jane";
    const StringTwo = "Mikea";
    const count = countChangesInString(stringOne, StringTwo);
    expect(count).toBe(4);
  });

  it("When countChangesInString Called with 0 differences Should Return Correct Value 0", () => {
    const stringOne = "Jane";
    const StringTwo = "Jane";
    const count = countChangesInString(stringOne, StringTwo);
    expect(count).toBe(0);
  });
});
