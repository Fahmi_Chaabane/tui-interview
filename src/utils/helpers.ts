import { Passenger } from "./../types/Passenger";

export const calculateAge = (dateString: string) => {
  var today = new Date();
  var birthDate = new Date(dateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
};

export const setupAgesCategories = (passengers: Record<number, Passenger>) => {
  const agesCategories = {
    adult: 0,
    child: 0,
    infant: 0,
  };

  for (let key in passengers) {
    const age = calculateAge(passengers[key]["date-of-birth"]);
    if (age > 18) {
      agesCategories.adult++;
    } else if (age > 6 && age < 18) {
      agesCategories.child++;
    } else {
      agesCategories.infant++;
    }
  }

  return agesCategories;
};

export const countChangesInString = (
  previousString: string,
  newString: string
) => {
  if (!previousString.length) return newString.length;
  if (!newString.length) return previousString.length;

  let matrix: number[][] = [];

  // increment along the first column of each row
  for (let i = 0; i <= previousString.length; i++) {
    matrix[i] = [i];
  }

  // increment each column in the first row
  for (let j = 0; j <= newString.length; j++) {
    matrix[0][j] = j;
  }

  // Fill in the rest of the matrix
  for (let i = 1; i <= previousString.length; i++) {
    for (let j = 1; j <= newString.length; j++) {
      if (previousString.charAt(i - 1) === newString.charAt(j - 1)) {
        matrix[i][j] = matrix[i - 1][j - 1];
      } else {
        matrix[i][j] = Math.min(
          matrix[i - 1][j - 1] + 1, // substitution
          Math.min(
            matrix[i][j - 1] + 1, // insertion
            matrix[i - 1][j] + 1
          )
        ); // deletion
      }
    }
  }

  return matrix[previousString.length][newString.length];
};
