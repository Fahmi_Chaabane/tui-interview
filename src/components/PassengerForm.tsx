import { ChangeEvent } from "react";
import { Stack } from "@mui/material";
import { PassengerTypeKeys, Passenger } from "../types/Passenger";
import { Title } from "../types/Title";
import { Gender } from "../types/Gender";
import { countChangesInString } from "../utils/helpers";
import {
  Control,
  FieldErrors,
  FieldValues,
  UseFormRegister,
  UseFormSetError,
  UseFormSetValue,
} from "react-hook-form";
import { DateField, InputField, SelectField } from "./fields";

export type IPassengerForm = {
  passenger: Passenger;
  index: string;
  register: UseFormRegister<FieldValues>;
  control: Control<FieldValues>;
  errors: FieldErrors<FieldValues>;
  setError: UseFormSetError<FieldValues>;
  setValue: UseFormSetValue<FieldValues>;
};

export default function PassengerForm({
  passenger,
  index,
  register,
  control,
  errors,
  setValue,
  setError,
}: IPassengerForm) {
  const handleTextFieldChange = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const newValue = e.target.value;
    const targetName = e.target.name;
    if (!newValue) {
      setError(targetName, { message: "Cannot be empty" });
      setValue(targetName, newValue);
      return;
    }

    const count = countChangesInString(
      passenger[targetName.split(".")[1] as PassengerTypeKeys],
      newValue
    );
    if (count > 3) {
      setError(targetName, { message: "Cannot do more than 3 changes" });
      setValue(targetName, newValue);
      return;
    }

    setError(targetName, { message: "" });
    setValue(targetName, newValue);
  };

  return (
    <Stack
      sx={{
        padding: "1rem 2.5rem",
        background: "#fff",
        marginBottom: "2rem",
      }}
      gap={3}
      component="form"
    >
      <Stack direction="row" spacing={4}>
        <SelectField
          control={control}
          options={Object.values(Title).map((v) => ({ label: v, value: v }))}
          name={`${index}.title`}
          labelId="title-label"
          id="title"
          label="Title"
        />
        <SelectField
          control={control}
          options={Object.values(Gender).map((v) => ({ label: v, value: v }))}
          name={`${index}.gender`}
          labelId="gender-label"
          id="gender"
          label="Gender"
        />
      </Stack>

      <Stack direction="row" spacing={8}>
        <InputField
          name={`${index}.firstName`}
          register={register}
          onChange={handleTextFieldChange}
          id="firstName"
          label="First Name"
          sx={{ flex: 1 }}
          error={!!errors[index]?.firstName?.message}
          helperText={errors[index]?.firstName?.message}
        />

        <InputField
          name={`${index}.lastName`}
          register={register}
          id="lastName"
          label="Last Name"
          onChange={handleTextFieldChange}
          sx={{ flex: 1 }}
          error={!!errors[index]?.lastName?.message}
          helperText={errors[index]?.lastName?.message}
        />
      </Stack>

      <DateField control={control} name={`${index}.date-of-birth`} />
    </Stack>
  );
}
