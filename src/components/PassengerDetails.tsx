import { FC } from "react";
import { Passenger } from "../types/Passenger";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  TextField,
} from "@mui/material";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import moment from "moment";
import { Title } from "../types/Title";
import { Gender } from "../types/Gender";

type IPassengerDetails = {
  passenger: Passenger;
};

export const PassengerDetails: FC<IPassengerDetails> = ({ passenger }) => {
  return (
    <Stack
      sx={{
        padding: "1rem 2.5rem",
        background: "#fff",
        marginBottom: "2rem",
      }}
      gap={3}
      component="form"
    >
      <Stack direction="row" spacing={4}>
        <FormControl>
          <InputLabel id="select-label">Title</InputLabel>

          <Select
            defaultValue={passenger.title}
            labelId="title-label"
            id="title"
            label="Title"
            readOnly={true}
          >
            {Object.values(Title).map((v) => (
              <MenuItem key={v} value={v}>
                {v}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl>
          <InputLabel id="select-label">Gender</InputLabel>
          <Select
            defaultValue={passenger.gender}
            labelId="gender-label"
            id="gender"
            label="Gender"
            readOnly={true}
          >
            {Object.values(Gender).map((v) => (
              <MenuItem key={v} value={v}>
                {v}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Stack>

      <Stack direction="row" spacing={8}>
        <TextField
          defaultValue={passenger.firstName}
          id="firstName"
          label="First Name"
          sx={{ flex: 1 }}
          InputProps={{
            readOnly: true,
          }}
        />

        <TextField
          defaultValue={passenger.lastName}
          id="lastName"
          label="Last Name"
          sx={{ flex: 1 }}
          InputProps={{
            readOnly: true,
          }}
        />
      </Stack>
      <LocalizationProvider dateAdapter={AdapterMoment}>
        <DatePicker
          value={moment(passenger["date-of-birth"])}
          readOnly={true}
        />
      </LocalizationProvider>
    </Stack>
  );
};
