import { FC } from "react";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectProps,
  SxProps,
} from "@mui/material";
import { Control, Controller, FieldValues } from "react-hook-form";

type IOption = { label: string; value: string };

type ISelectField = SelectProps & {
  control: Control<FieldValues>;
  options: IOption[];
  name: string;
  sx?: SxProps;
};

export const SelectField: FC<ISelectField> = ({
  control,
  name,
  options,
  sx,
  label,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <FormControl>
          <InputLabel id="select-label">{label}</InputLabel>

          <Select label={label} {...field} sx={sx} {...rest}>
            {options.map((op) => (
              <MenuItem key={op.label} value={op.value}>
                {op.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      )}
    />
  );
};
