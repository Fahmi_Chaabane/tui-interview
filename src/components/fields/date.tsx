import { FC } from "react";
import { SxProps } from "@mui/material";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import { Control, Controller, FieldValues } from "react-hook-form";
import moment from "moment";

type IDateField = {
  control: Control<FieldValues>;
  name: string;
  readOnly?: boolean;
  sx?: SxProps;
};

export const DateField: FC<IDateField> = ({ control, name, readOnly, sx }) => {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <LocalizationProvider dateAdapter={AdapterMoment}>
          <DatePicker
            value={moment(field.value)}
            onChange={(e) => field.onChange(e?.format("YYYY-MM-DD"))}
            sx={sx}
            readOnly={readOnly}
          />
        </LocalizationProvider>
      )}
    />
  );
};
