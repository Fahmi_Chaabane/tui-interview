import { FC } from "react";
import { TextField, TextFieldProps, SxProps } from "@mui/material";
import { FieldValues, UseFormRegister } from "react-hook-form";

type IInputField = TextFieldProps & {
  name: string;
  register: UseFormRegister<FieldValues>;
  sx?: SxProps;
};

export const InputField: FC<IInputField> = ({
  sx,
  name,
  register,
  ...rest
}) => {
  return (
    <TextField
      variant="standard"
      {...register(name, { required: true })}
      sx={sx}
      {...rest}
    />
  );
};
