import { Dispatch, SetStateAction, useState } from "react";
import {
  Typography,
  Button,
  Box,
  Stack,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Alert,
  AlertTitle,
} from "@mui/material";
import { Passenger } from "../types/Passenger";
import { PassengerDetails } from "./PassengerDetails";

type IConfirmationDialog = {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  originalPassengers: Record<number, Passenger>;
  newPassagers: Record<number, Passenger>;
};

export default function ConfirmationDialog({
  open,
  setOpen,
  originalPassengers,
  newPassagers,
}: IConfirmationDialog) {
  const [showAlert, setshowAlert] = useState<boolean>(false);

  const handleClose = () => {
    setOpen(false);
    setshowAlert(false);
  };

  const handleSubmit = () => {
    // simulate a backend call.
    let count = localStorage.getItem("requestCount");
    if (!count) {
      localStorage.setItem("requestCount", "1");
      handleClose();
    } else if (+count >= 1) {
      setshowAlert(true);
    }
  };

  return (
    <>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth="lg"
      >
        <DialogTitle id="alert-dialog-title">
          Confirm you modification
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            id="alert-dialog-description"
            bgcolor="#e2f3fe"
            padding="2rem"
          >
            {showAlert && (
              <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                You have surpassed
                <strong> the maximum of allowed changes!</strong>
              </Alert>
            )}
            <Stack direction="row" spacing={8}>
              <Box>
                <Typography
                  fontWeight={600}
                  color="#1b115c"
                  fontSize="1.2rem"
                  marginBottom="1rem"
                >
                  Original Input
                </Typography>
                {Object.entries(originalPassengers).map(([key, value]) => (
                  <div key={key}>
                    <PassengerDetails passenger={value} />
                  </div>
                ))}
              </Box>
              <Box>
                <Typography
                  fontWeight={600}
                  color="#1b115c"
                  fontSize="1.2rem"
                  marginBottom="1rem"
                >
                  New Input
                </Typography>
                {Object.entries(newPassagers).map(([key, value]) => (
                  <div key={key}>
                    <PassengerDetails passenger={value} />
                  </div>
                ))}
              </Box>
            </Stack>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSubmit} autoFocus>
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
