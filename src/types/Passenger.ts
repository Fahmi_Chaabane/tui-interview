import { Gender } from "./Gender";
import { Title } from "./Title";

export type Passenger = {
  title: Title;
  gender: Gender;
  firstName: string;
  lastName: string;
  "date-of-birth": string;
};

export type PassengerTypeKeys = keyof Passenger;
