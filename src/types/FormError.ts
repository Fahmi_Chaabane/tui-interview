export type FormError = {
  title: string;
  gender: string;
  firstName: string;
  lastName: string;
  "date-of-birth": string;
};

export type FormErrorTypeKeys = keyof FormError;
